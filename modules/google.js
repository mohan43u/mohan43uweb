/*
 * google.js - simple google client
 */

var fs = require('fs');
var HttpClient = require('./httpclient.js');

var Google = function(props) {
    props = props || {};
    for(var prop in props) this[prop] = props[prop];
    this.client_id = this.client_id || '27591029818-7dilpplml7eor71bu6jbck6qdru47894.apps.googleusercontent.com';
    this.client_secret = this.client_secret || 'N4jv1C0pwFjRunvgGrtLpbxI';
    this.redirect_uris = this.redirect_uris || ['https://mohan43uweb-silentcrooks.rhcloud.com/google/authorized'];
    this.auth_uri = this.auth_uri || 'https://accounts.google.com/o/oauth2/auth';
    this.token_uri = this.token_uri || 'https://accounts.google.com/o/oauth2/token';
    this.scope = this.scope || 'https://www.googleapis.com/auth/drive';
    this.api_root = this.api_root || 'https://www.googleapis.com';
};

Google.prototype.init = function(request, reply) {
    if(request.params.route == 'authorized') {
	this.get_token(request, function(reply, error, original_uri) {
	    reply.redirect(error ? '/error?error=' + (new Buffer(JSON.stringify(error))).toString('base64') : original_uri);
	}.bind(this, reply));
    }
    else {
	var url = this.auth_uri + '?client_id=' + this.client_id;
	url += '&redirect_uri=' + this.redirect_uris[0];
	url += '&scope=' + encodeURIComponent(this.scope);
	url += '&state=' + encodeURIComponent(request.url.href);
	url += '&access_type=offline';
	url += '&response_type=code';
	return reply.redirect(url);
    }
};

Google.prototype.get_token = function(request, callback) {
    var code = request.url.href.match(/code=([^&]*)/)[1];
    var original_uri = decodeURIComponent(request.url.href.match(/state=([^&]*)/)[1]);
    var form = { 'client_id': this.client_id,
		 'client_secret': this.client_secret,
		 'redirect_uri': this.redirect_uris[0],
		 'code': code,
		 'grant_type': 'authorization_code' };
    var opts = { method: 'POST',
		 url: this.token_uri,
		 form: form };
    var httpclient = new HttpClient(opts);
    httpclient.call(function(callback, original_uri, error, response, body) {
	if(!error && response.statusCode == 200) {
	    var result = JSON.parse(body);
	    for(var prop in result) this[prop] = result[prop];
	    fs.writeFileSync(google_cfg_path, JSON.stringify(this));
	}
	callback(error, original_uri);
    }.bind(this, callback, original_uri));
};

Google.prototype.handle_expiration = function(original_httpclient, callback) {
    var form = { 'client_id': this.client_id,
		 'client_secret': this.client_secret,
		 'refresh_token': this.refresh_token,
		 'grant_type': 'refresh_token' };
    var opts = { method: 'POST',
		 url: this.token_uri,
		 form: form };
    var httpclient = new HttpClient(opts);
    httpclient.call(function(callback, original_httpclient, error, response, body) {
	if(error || response.statusCode != 200) {
	    callback(error, response, body);
	    return;
	}
	var result = JSON.parse(body);
	for(var prop in result) this[prop] = result[prop];
	fs.writeFileSync(google_cfg_path, JSON.stringify(this));
	original_httpclient.opts.headers['authorization'] = 'Bearer ' + this.access_token;
	original_httpclient.call(callback);
    }.bind(this, callback, original_httpclient));
};

Google.prototype.call = function(request, reply) {
    var uri = undefined;
    if(request.params.route == 'download') {
	uri = (new Buffer(request.params.path, 'base64')).toString();
    }
    else {
	uri = request.url.href.substring(1);
	uri = uri.substring(uri.indexOf('/'));
	uri = this.api_root + uri;
    }
    var opts = { method: request.method,
		 url: uri,
		 headers: request.headers };
    opts.headers['authorization'] = 'Bearer ' + this.access_token;
    var httpclient = new HttpClient({opts: opts, payload: request.payload});
    httpclient.call(function(reply, httpclient, error, response, body) {
	if(error) return reply(error);
	var send = function(reply, error, response, body) {
	    if(error) return reply(error);
	    var res = reply(body);
	    res.code(response.statusCode);
	    for(var prop in response.headers) res.header(prop, response.headers[prop]);
	}.bind(this, reply);
	if( response.statusCode == 401) {
	    this.handle_expiration(httpclient, function(send, error, response, body) {
		send(error, response, body);
	    }.bind(this, send));
	}
	else {
	    send(error, response, body);
	}
    }.bind(this, reply, httpclient));
};

module.exports = Google;
