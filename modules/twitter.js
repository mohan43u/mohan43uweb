/*
 * twitter.js - simple twitter client
 */

var fs = require('fs');
var HttpClient = require('./httpclient.js');

var Twitter = function(props) {
    props = props || {};
    for(var prop in props) this[prop] = props[prop];
    this.consumer_key = this.consumer_key || 'iyt0UckNMQn0rbA8QP26i02v3';
    this.consumer_secret = this.consumer_secret || 'nzPUYsQt69SMqHDU8AJ7vRJYBvwS9AehgBSyMxmylHYuwKoARF';
};

Twitter.prototype.new_http_client = function(request) {
    if(typeof(request) == 'string') request = { method: 'GET', uri: request };
    if(request.url && request.url.href) {
	request.uri = request.url.href.substring(1);
	request.uri = request.uri.substring(request.uri.indexOf('/') + 1);
    }
    var opts = { method: request.method,
		 url: request.uri,
		 headers: request.headers,
		 oauth: {
		     consumer_key: this.consumer_key,
		     consumer_secret: this.consumer_secret,
		     token: this.access_token,
		     token_secret: this.access_token_secret
		 }};
    return new HttpClient({opts: opts, payload: request.payload});
}

Twitter.prototype.call = function(request, reply) {
    var httpclient = this.new_http_client(request);
    var stream = httpclient.stream();
    stream.on('data', function(chunk) { this.raw.res.write(chunk); }.bind(request));
    stream.on('end', function(reply, chunk) { this.raw.res.end(chunk); reply.close({end: false}); }.bind(request, reply));
    request.raw.req.socket.on('close', function() { this.abort(); }.bind(stream));
};

Twitter.prototype.io = function(socket, request) {
    var id = request.id;
    if(!this.ioclients) this.ioclients = {};
    if(!this.ioclients[socket.id]) this.ioclients[socket.id] = {};
    if(request.data == 'disconnect') return this.ioclients[socket.id][request.id] && this.ioclients[socket.id][request.id].abort();
    var httpclient = this.new_http_client(request.data);
    var stream = httpclient.stream();
    this.ioclients[socket.id][request.id] = stream;
    var response_handler = function(id, chunk) { this.emit(id, {id: id, data: chunk}); }.bind(socket, id);
    stream.on('data', response_handler);
    stream.on('end', response_handler);
    stream.on('error', function(id, error) { this.emit(id, {id: id, data: new Buffer(JSON.stringify(error))}); }.bind(socket, request.id));
    socket.on('disconnect', function() { this.abort(); }.bind(stream));
};

Twitter.prototype.gettrends = function(socket, request) {
    var yahoouri = 'http://where.yahooapis.com/v1/places.q(';
    yahoouri += request.data;
    yahoouri += ')?appid=sU2cwl7V34FjV_Wsry3dW2Vd0rk6A18PSPUMznLh_VdRC8E_yrH8iOu4SPDaEE4-&format=json&view=short';
    var yahooclient = new HttpClient({method: 'GET', uri: yahoouri});
    yahooclient.call(function(socket, request, error, response, body) {
	if(error) return socket.emit(request.id, {id: request.id, data: new Buffer(JSON.stringify(error))});
	var woeid = JSON.parse(body.toString())
	console.log(woeid);
	woeid = (woeid && woeid.places && woeid.places.place && woeid.places.place[0] && woeid.places.place[0].woeid ? woeid.places.place[0].woeid : 1);
	var httpclient = this.new_http_client('https://api.twitter.com/1.1/trends/place.json?id=' + woeid);
	var stream = httpclient.stream();
	var response_handler = function(id, chunk) { this.emit(id, {id: id, data: chunk}); }.bind(socket, request.id);
	stream.on('data', response_handler);
	stream.on('end', response_handler);
	stream.on('error', function(id, error) { this.emit(id, {id: id, data: new Buffer(JSON.stringify(error))}); }.bind(socket, request.id));
	socket.on('disconnect', function() { this.abort(); }.bind(stream));
    }.bind(this, socket, request));
};

module.exports = Twitter;
