/*
 * socket.js - simple socket.io
 */

//process.env.DEBUG = process.env.DEBUG || "engine:*";
SocketIO = require('socket.io');

var Socket = function(props) {
    props = props || {};
    for(var prop in props) this[prop] = props[prop];
};

Socket.prototype.setup_handler = function(namespace, handler) {
    this.io.of(namespace).on('connection', handler);
}

Socket.prototype.listen = function(listener, opts) {
    this.io = (new SocketIO()).attach(listener ||
				      this.listener ||
				      require('http')
				      .createServer()
				      .listen(process.env.OPENSHIFT_NODEJS_IP,
					      process.env.OPENSHIFT_NODEJS_PORT));
};

module.exports = Socket;
