/*
 * app.js - server
 */

var fs = require('fs');
var Hapi = require('hapi');
var Google = require('./google.js');
var Socket = require('./socket.js');
var Twitter = require('./twitter.js');
var WordPress = require('./wordpress.js');
var WebObjects = require('./webobjects.js');
var HttpClient = require('./httpclient.js');

wordpress_cfg_path = process.env.OPENSHIFT_DATA_DIR + '/wordpress.cfg';
google_cfg_path = process.env.OPENSHIFT_DATA_DIR + '/google.cfg';
twitter_cfg_path = process.env.OPENSHIFT_DATA_DIR + '/twitter.cfg';

var wordpress_cfg = fs.existsSync(wordpress_cfg_path) ? fs.readFileSync(wordpress_cfg_path, 'utf8') : '{}';
var google_cfg = fs.existsSync(google_cfg_path) ? fs.readFileSync(google_cfg_path, 'utf8') : '{}';
var twitter_cfg = fs.existsSync(twitter_cfg_path) ? fs.readFileSync(twitter_cfg_path, 'utf8') : '{}';
var wordpress = new WordPress(JSON.parse(wordpress_cfg));
var google = new Google(JSON.parse(google_cfg));
var socket = new Socket();
var twitter = new Twitter(JSON.parse(twitter_cfg));
var server = new Hapi.Server({debug: {log: ['error', 'load'], request: ['error', 'client']}});
var webobjects = new WebObjects();

var client = function(request, reply) {
    var clientinfo = {
	time: (new Date(request.info.received)).toLocaleString(),
	method: request.method,
	host: request.info.host,
	href: request.url.href,
	agent: request.headers['user-agent'],
	referer: request.info.referrer,
	ip: request.info.remoteAddress,
	port: request.info.remotePort
    };
    request.log(['client'], clientinfo);
    return reply();
};

var external = function(request, reply) {
    var uri = undefined;
    if(request.path == '/favicon.ico') {
	uri = 'http://www.gravatar.com/avatar/2ef790b80c1291101b5589eafcd665e4.png';
    }
    else {
	uri = request.params.uri;
	uri = (new Buffer(uri, 'base64')).toString();
    }
    var opts = { method: request.method,
		 uri: uri,
		 headers: request.headers };
    var httpclient = new HttpClient(opts);
    httpclient.call(function(reply, error, response, body) {
	if(error) return reply(error);
	var res = reply(body);
	res.code(response.statusCode);
	for(var prop in response.headers) res.header(prop, response.headers[prop]);
    }.bind(this, reply));
};

server.connection({
    host: process.env.OPENSHIFT_NODEJS_IP,
    port: process.env.OPENSHIFT_NODEJS_PORT
});
socket.listen(server.listener);

server.route({
    method: [ 'GET' ],
    path: '/wordpress/{path*}',
    config: { pre: [ client ] },
    handler: function(request, reply) {
	if(!wordpress.access_token) return wordpress.init(request, reply);
	wordpress.call(request, reply);
    }
});

server.route({
    method: [ 'GET' ],
    path: '/google/{route}/{path*}',
    config: { pre: [ client ] },
    handler: function(request, reply) {
	if(!google.access_token) return google.init(request, reply);
	google.call(request, reply);
    }
});

server.route({
    method: [ 'GET' ],
    path: '/twitter/{path*}',
    config: { pre: [ client ] },
    handler: function(request, reply) {
	twitter.call(request, reply);
    }
});
socket.setup_handler('twitter', function(twitter, socket) {
    socket.on('hometweets', twitter.io.bind(twitter, socket));
    socket.on('userstream', twitter.io.bind(twitter, socket));
    socket.on('publicstream', twitter.io.bind(twitter, socket));
    socket.on('search', twitter.io.bind(twitter, socket));
    socket.on('gettrends', twitter.gettrends.bind(twitter, socket));
}.bind(this, twitter));

server.route({
    method: '*',
    path: '/webobjects/{name*}',
    config: { pre: [ client ] },
    handler: function(request, reply) {
	webobjects.handle(request, reply);
    }
});

server.route({
    method: '*',
    path: '/cors/{uri*}',
    config: { pre: [ client ] },
    handler: external
});

server.route({
    method: '*',
    path: '/favicon.ico',
    config: { pre: [ client ] },
    handler: external
});

server.route({
    method: '*',
    path: '/{path*}',
    config: { pre: [ client ] },
    handler: function(request, reply) {
	if(request.params.path) {
	    var path = './statics/' + request.params.path;
	    reply.file(path);
	}
	else {
	    reply.file('./statics/html/index.html');
	}
    }
});

server.start(function() {
    console.log('server running at ' + server.info.uri);
});
