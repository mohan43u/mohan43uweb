/*
 * webobjects.js - serve the objects
 */

var fs = require('fs');

var WebObjects = function() {
}

WebObjects.prototype.handle = function(request, reply) {
    fs.readFile('./modules/webobjects/' + request.params.name + '.json', function(reply, error, data) {
	reply(error, data);
    }.bind(this, reply));
};

module.exports = WebObjects;
