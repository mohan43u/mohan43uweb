/*
 * wordpress.js - simple wordpress client
 */

var fs = require('fs');
var HttpClient = require('./httpclient.js');

var WordPress = function(props) {
    props = props || {};
    for(var prop in props) this[prop] = props[prop];
    this.client_id = this.client_id || '34195';
    this.client_secret = this.client_secret || 'h8R2sJVBBBAj9KTRUSOvh75jGARu04hQ0vlElQwKGBwSSUTtdPtNfLvc94aT98i1';
    this.redirect_uri = this.redirect_uri || 'https://mohan43uweb-silentcrooks.rhcloud.com/wordpress/authorized';
    this.authorize_uri = this.authorize_uri || 'https://public-api.wordpress.com/oauth2/authorize';
    this.token_uri = this.token_uri || 'https://public-api.wordpress.com/oauth2/token';
    this.scope = this.scope || 'global';
    this.api_root = this.api_root || 'https://public-api.wordpress.com/rest/v1.1';
};

WordPress.prototype.init = function(request, reply) {
    if(request.params.path == 'authorized') {
	this.get_token(request, function(reply, error, original_uri) {
	    reply.redirect(error ? '/error/' + new Buffer(JSON.stringify(error)).toString('base64') : original_uri);
	}.bind(this, reply));
    }
    else {
	var url = this.authorize_uri + '?client_id=' + this.client_id;
	url += '&redirect_uri=' + this.redirect_uri;
	url += '&scope=' + encodeURIComponent(this.scope);
	url += '&state=' + encodeURIComponent(request.url.href);
	url += '&response_type=code';
	return reply.redirect(url);
    }
};

WordPress.prototype.get_token = function(request, callback) {
    var code = request.url.href.match(/code=([^&]*)/)[1];
    var original_uri = decodeURIComponent(request.url.href.match(/state=([^&]*)/)[1]);
    var form = { 'client_id': this.client_id,
		 'client_secret': this.client_secret,
		 'redirect_uri': this.redirect_uri,
		 'code': code,
		 'grant_type': 'authorization_code' };
    var opts = { method: 'POST',
		 url: this.token_uri,
		 form: form };
    var httpclient = new HttpClient(opts);
    httpclient.call(function(callback, original_uri, error, response, body) {
	if(!error && response.statusCode == 200) {
	    var result = JSON.parse(body);
	    for(var prop in result) this[prop] = result[prop];
	    fs.writeFileSync(wordpress_cfg_path, JSON.stringify(this));
	}
	callback(error, original_uri);
    }.bind(this, callback, original_uri));
};

WordPress.prototype.call = function(request, reply) {
    var uri = request.url.href.substring(1);
    var uri = uri.substring(uri.indexOf('/'));
    var opts = { method: request.method,
		 url: this.api_root + uri,
		 headers: request.headers };
    opts.headers['authorization'] = 'Bearer ' + this.access_token;
    var httpclient = new HttpClient({opts: opts, payload: request.payload});
    httpclient.call(function(reply, error, response, body) {
	if(error) return reply(error);
	var res = reply(body);
	res.code(response.statusCode);
	for(var prop in response.headers) res.header(prop, response.headers[prop]);
    }.bind(this, reply));
};

module.exports = WordPress;
