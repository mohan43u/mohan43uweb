/*
 * httpclient.js - wrapper around request
 */

//process.env.NODE_DEBUG = process.env.NODE_DEBUG || 'request'
var request = require('request');

var HttpClient = function(props) {
    this.opts = props.opts || props;
    this.payload = props.payload;
};

HttpClient.prototype.parse_payload = function(form, payload) {
    var addpart = function(form, key, value, options) {
	if(options) {
	    form[key] = { value: value, options: options};
	}
	else if(typeof(value) == "string" || typeof(value) == "number") {
	    form[key] = value;
	}
	else {
	    for(var prop in value) {
		form[key + '[' + prop + ']'] = value[prop];
	    }
	}
    }
    for(var partname in payload) {
	var part = payload[partname];
	if(part instanceof Array) {
	    for(var index in part) {
		var key = partname + '[' + index + ']';
		var value = part[index];
		var options = undefined;
		if(partname == 'media') options = payload['attrs'][index];
		addpart(form, key, value, options);
	    }
	}
	else {
	    addpart(form, partname, part);
	}
    }
};

HttpClient.prototype.parse_opts = function() {
    if(this.opts.headers) delete this.opts.headers['host'];
    if(this.opts.headers) delete this.opts.headers['content-length'];
    this.opts.strictSSL = false;
    this.opts.gzip = true;
    this.opts.encoding = null;
    if(process.env['http_proxy']) this.opts.proxy = 'http://' + process.env['http_proxy'];
    if(this.opts.headers
       && this.opts.headers['content-type']
       && this.opts.headers['content-type'].search('application/x-www-form-urlencoded') != -1
       && this.payload
       && !this.opts.form) {
	this.opts.form = {};
	this.parse_payload(this.opts.form, this.payload);
    }
    if(this.opts.headers
       && this.opts.headers['content-type']
       && this.opts.headers['content-type'].search('multipart/') != -1
       && this.payload
       && !this.opts.formData) {
	this.opts.formData = this.opts.formData || {};
	this.parse_payload(this.opts.formData, this.payload);
    }
};

HttpClient.prototype.call = function(callback) {
    this.parse_opts();
    request(this.opts, function(callback, error, response, body) {
	if(response
	   && response.headers
	   && response.headers['content-encoding']) {
	    delete response.headers['content-encoding'];
	}
	callback(error, response, body);
    }.bind(this, callback));
};

HttpClient.prototype.stream = function() {
    this.parse_opts();
    return request(this.opts);
}

module.exports = HttpClient;
