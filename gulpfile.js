/*
 * gulpfile.js - gulp configuration
 */

var gulp = require('gulp');
var handlebars = require('gulp-handlebars');
var wrap = require('gulp-wrap');
var declare = require('gulp-declare');
var concat = require('gulp-concat');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var stylus = require('gulp-stylus');
var nib = require('nib');
var jeet = require('jeet');


gulp.task('default', ['handlebars', 'stylus', 'browserify'], function() {});

gulp.task('handlebars', function() {
    gulp.src('statics/handlebars/*.hb')
	.pipe(handlebars())
	.pipe(wrap('handlebars.template(<%= contents %>)'))
	.pipe(declare({root: 'module.exports', noReclare: true}))
	.pipe(concat('hbstatic.js'))
	.pipe(wrap("var handlebars = require('handlebars');\n <%= contents %>"))
	.pipe(gulp.dest('statics/js'));
});

gulp.task('stylus', function() {
    gulp.src('statics/stylus/*.styl')
	.pipe(stylus({use: [jeet(), nib()]}))
	.pipe(gulp.dest('statics/css'));
});

gulp.task('browserify', function() {
    var bundler = browserify({require: ['./statics/js/deps.js'], exposeAll: true});
    bundler.bundle()
	.pipe(source('bundle.js'))
	.pipe(gulp.dest('statics/js'));
});
