/*
 * comments.js - show comments
 */

var httpclient = require('./httpclient.js');
var hb = require('./hb.js');
var utils = require('./utils.js');

var Comments = function(props) {
    props = props || {};
    this.classname = props.classname || 'comments';
    this.id = props.id || ('comments' + (Comments.count++));
};

Comments.count = 0;

Comments.prototype.render_inside = function(parent, next) {
    this.view = parent;
    this.view.id = this.id;
    this.view.className = this.classname;
    this.view.webObject = this;
    var post = this.view.parentNode.webObject;
    this.uri = '/wordpress/sites/' + post.site_ID + '/posts/' + post.ID + '/replies';
    this.get_comments(next);
};

Comments.prototype.get_comments = function(next) {
    httpclient({method: 'GET', uri: this.uri, json: true}, function(next, error, response, body){
	if(!error && response.statusCode == 200) {
	    this.comments = body;
	    if(!this.found) this.found = this.comments.found;
	    if(!this.fetched) this.fetched = 0;
	    this.fetched += this.comments.comments.length;
	    this.comments.comments.forEach(function(comment) {
		if(this.view.querySelector('#comment' + comment.ID)) return;
		if(comment.status == "approved") {
		    var element = utils.attach(this.view, hb.comment({id: 'comment' + comment.ID, classname: 'comment', comment: comment}));
		    element.webObject = comment;
		}
	    }.bind(this));
	    this.add_more_button();
	    if(next) next();
	}
	else {
	    console.warn({error: error, response: response, body: body});
	}
    }.bind(this, next));
};

Comments.prototype.add_more_button = function() {
    if(this.fetched < this.found) {
	var morebutton = document.createElement('a');
	morebutton.className = 'more';
	morebutton.href = "javascript:void(0)";
	morebutton.textContent = 'Older Comments..';
	this.view.appendChild(morebutton);
	morebutton.addEventListener('click', function(morebutton) {
	    this.view.removeChild(morebutton);
	    this.uri = this.uri.substring(0, (this.uri.indexOf('?') < 0 ? this.uri.length : this.uri.indexOf('?')));
	    this.uri += '?before=' + encodeURIComponent(this.comments.comments[this.comments.comments.length - 1].date);
	    this.get_comments();
	}.bind(this, morebutton));
    }
};

module.exports = Comments;
