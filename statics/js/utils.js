/*
 * utilities functions
 */

var attach = function(parent, html, insert) {
    var element = document.createElement('div');
    if(typeof(parent) == 'string') parent = document.getElementById(parent);
    if(!parent) console.warn(Error(parent_id + ' not in DOM'));
    if(html) {
	element.innerHTML = html;
	element = element.firstElementChild;
    }
    if(insert) {
	parent.insertBefore(element, parent.firstChild);
    }
    else {
	parent.appendChild(element);
    }
    return element;
}

module.exports = { attach: attach };
