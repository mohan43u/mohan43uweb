/*
 * dependencies.js - browserify dependencies
 */

module.exports.config = require('../../modules/config.js');
module.exports.widgets = require('./widgets.js');
