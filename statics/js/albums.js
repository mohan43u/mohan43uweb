/*
 * albums.js - album view
 */

var httpclient = require('./httpclient.js');
var hb = require('./hb.js');
var utils = require('./utils.js');

var Albums = function(props) {
    props = props || {};
    this.classname = props.classname || "albums";
    this. id = props.id || ("albums" + (Albums.count++));
    this.path_root = props.path_root || '/google/drive/v2/files/';
    this.download_root = props.download_root || '/google/download/';
    this.path = props.path || this.path_root + "0B6Q-hqxJjfH7aUxlV2NEQXUzZWM/children";
};

Albums.count = 0;

Albums.prototype.render_inside = function(parent, next) {
    var dmain = document.getElementById('dmain');
    this.view = utils.attach(dmain);
    this.view.id = this.id;
    this.view.className = this.classname;
    var albumscontainer = utils.attach(this.view);
    albumscontainer.id = 'albumscontainer';
    var albumscontentcontainer = utils.attach(this.view);
    albumscontentcontainer.id = 'albumscontentcontainer';
    this.view.webObject = this;
    if(!dmain.webObject) dmain.webObject = this;
    if(dmain.webObject != this) this.hide();
    httpclient({method: 'GET', uri: this.path, json: true}, function(next, error, response, body) {
	if(!error && response.statusCode == 200) {
	    this.albums = body;
	    this.albums.items.forEach(function(next, album) {
		this.get_album(album, next);
	    }.bind(this, next));
	}
	else {
	    console.warn({error: error, response: response, body: body});
	}
    }.bind(this, next));
    return this;
};

Albums.prototype.get_album = function(album, next) {
    var uri = this.path_root + album.id;
    httpclient({method: 'GET', uri: uri, json: true} , function(next, error, response, body) {
	if(!error && response.statusCode == 200) {
	    var album = body;
	    var element = utils.attach(this.view.querySelector('#albumscontainer'), hb.album({id: album.id, classname: 'album', album: album}));
	    element.webObject = album;
	    this.add_pictures(element);
	}
	else {
	    console.warn({error: error, response: response, body: body});
	}
	this.rendered = true;
	if(next) next();
    }.bind(this, next));
};

Albums.prototype.add_pictures = function(album) {
    album.addEventListener('click', function(album) {
	var albumscontentcontainer = this.view.querySelector('#albumscontentcontainer');
	var albumcontent = albumscontentcontainer.querySelector('#' + album.id.substring(album.id.lastIndexOf('-') + 1));
	if(albumscontentcontainer.currentContent) {
	    if(albumscontentcontainer.currentContent == albumcontent) return;
	    albumscontentcontainer.currentContent.style.display = 'none';
	}
	if(albumcontent) {
	    if(albumcontent.style.display == 'none') albumcontent.style.display = 'block';
	    lightbox.thumbnails = albumcontent.thumbnails;
	    albumscontentcontainer.currentContent = albumcontent;
	    return;
	}
	var uri = this.path_root + album.id + '/children';
	albumcontent = utils.attach(albumscontentcontainer);
	albumcontent.id = album.id.substring(album.id.lastIndexOf('-') + 1);
	albumcontent.className = 'albumcontent';
	var picturescontainer = utils.attach(albumcontent);
	picturescontainer.id = 'picturescontainer';
	var morebuttoncontainer = utils.attach(albumcontent);
	morebuttoncontainer.id = 'morebuttoncontainer';
	albumcontent.thumbnails = [];
	lightbox.thumbnails = albumcontent.thumbnails;
	albumscontentcontainer.currentContent = albumcontent;
	this.get_pictures(uri, albumcontent);
    }.bind(this, album.webObject));
    
};

Albums.prototype.get_pictures = function(uri, albumcontent) {
    httpclient({method: 'GET', uri: uri, json: true}, function(uri, albumcontent, error, response, body) {
	if(!error && response.statusCode == 200) {
	    body.items.forEach(function(albumcontent, picture) {
		this.add_picture(albumcontent, picture);
	    }.bind(this, albumcontent));
	    this.add_more_button(uri, body, albumcontent);
	}
	else {
	    console.warn({error: error, response: response, body: body});
	}
    }.bind(this, uri, albumcontent));
}

Albums.prototype.add_picture = function(albumcontent, picture) {
    httpclient({method: 'GET', uri: this.path_root + picture.id, json: true}, function(albumcontent, error, response, body) {
	var picture = body;
	var element = utils.attach(albumcontent.querySelector('#picturescontainer'),
				   hb.picture({id: picture.id,
					       classname: 'picture',
					       small: picture.thumbnailLink,
					       big: picture.webContentLink}));
	element.webObject = picture;
	if(!lightbox.box) {
	    lightbox.load({preload: false});
	}
	else {
	    albumcontent.thumbnails.push(element);
	    element.addEventListener('click', function(){ lightbox.open(this); }.bind(element));
	}
    }.bind(this, albumcontent));
};

Albums.prototype.add_more_button = function(uri, body, albumcontent) {
    if(!(body && body.nextPageToken)) return;
    var morebutton = document.createElement('a');
    morebutton.className = 'more';
    morebutton.href = "javascript:void(0)";
    morebutton.textContent = 'More..';
    albumcontent.querySelector('#morebuttoncontainer').appendChild(morebutton);
    morebutton.addEventListener('click', function(body, morebutton, uri, albumcontent) {
	morebutton.parentNode.removeChild(morebutton);
	var index = uri.indexOf('?');
	uri = (index > 0 ? uri.substring(0, index) : uri) + '?pageToken=' + body.nextPageToken;
	this.get_pictures(uri, albumcontent);
    }.bind(this, body, morebutton, uri, albumcontent));
};

Albums.prototype.clicked = function() {
    if(!this.rendered) return;
    this.show();
};

Albums.prototype.show = function() {
    var dmain = document.getElementById('dmain');
    dmain.webObject.hide();
    dmain.webObject = this;
    if(this.view.style.display == 'none') this.view.style.display = 'block';
};

Albums.prototype.hide = function() {
    this.view.style.display = 'none';
};

module.exports = Albums;
