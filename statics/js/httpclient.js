/*
 * request.js - overlays browser-request
 */

var request = require('browser-request');

module.exports = function(opts, callback) {
    request(opts, function(callback, error, response, body) {
	if(!(response.statusCode == 403 || (error && error.cors))) return callback(error, response, body);
	opts.uri = '/cors/' + btoa(opts.uri);
	request(opts, callback);
    }.bind(this, callback));
};
