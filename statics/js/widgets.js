/*
 * collect all the widgets here
 */

module.exports.list = require('./list.js');
module.exports.posts = require('./posts.js');
module.exports.comments = require('./comments.js');
module.exports.albums = require('./albums.js');
module.exports.tweets = require('./tweets.js');
