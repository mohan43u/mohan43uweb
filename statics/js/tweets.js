/*
 * Tweets.js - twitter view
 */

var hb = require('./hb.js');
var utils = require('./utils.js');
//localStorage.debug = "socket.io-client:socket";

var Tweets = function(props) {
    props = props || {};
    this.classname = props.classname || 'tweets';
    this.id = props.id || ('tweets' + (Tweets.count++));
    this.namespace = props.namespace || 'https://mohan43uweb-silentcrooks.rhcloud.com:8443/twitter';
    this.api_root = props.api_root || 'https://api.twitter.com/1.1/';
    this.stream_api_roots = props.stream_api_roots || { 'public': 'https://stream.twitter.com/1.1/statuses/filter.json', 'user': 'https://userstream.twitter.com/1.1/user.json' }
};

Tweets.count = 0;

Tweets.prototype.render_inside = function(parent, next) {
    var dmain = document.getElementById('dmain');
    this.view = utils.attach(dmain);
    this.view.id = this.id;
    this.view.className = this.classname;
    this.view.webObject = this;
    if(!dmain.webObject) dmain.webObject = this;
    if(dmain.webObject != this) this.hide();
    var options = utils.attach(this.view);
    options.id = 'options';
    var content = utils.attach(this.view);
    content.id = 'content';
    this.load_options(options, content);
    this.rendered = true;
    if(next) next();
};

Tweets.prototype.load_options = function(options, content) {
    var home = utils.attach(options);
    home.id = 'home';
    home.innerHTML = '<a href="javascript:void(0)">' + home.id + '</a>'
    var trends = utils.attach(options);
    trends.id = 'trends';
    trends.innerHTML = '<a href="javascript:void(0)">' + trends.id + '</a>';
    content.socket = io(this.namespace);
    var init = function(option, content) {
	option.addEventListener('click', function(option, content) {
	    var optioncontent = content.querySelector('#' + option.id);
	    if(content.currentContent) {
		if(content.currentContent == optioncontent) return;
		content.currentContent.style.display = 'none';
	    }
	    if(optioncontent) {
		if(optioncontent.style.display = 'none') optioncontent.style.display = 'block';
		content.currentContent = optioncontent;
		return;
	    }
	    optioncontent = utils.attach(content);
	    optioncontent.id = option.id;
	    this.load_option(optioncontent);
	    content.currentContent = optioncontent;
	}.bind(this, option, content));
    }.bind(this);
    init(home, content);
    init(trends, content);
    home.click();
};

Tweets.prototype.load_option = function(optioncontent) {
    if(optioncontent.id == 'home') this.load_home(optioncontent);
    if(optioncontent.id == 'trends') this.load_trends(optioncontent);
};

Tweets.prototype.load_home = function(optioncontent) {
    var request = {id: 'hometweets', data: this.api_root + '/statuses/home_timeline.json'};
    var realtime_req = {id: 'userstream', data: this.stream_api_roots['user']};
    var response = [];
    var realtime_res = [];
    optioncontent.socket = optioncontent.parentNode.socket;
    optioncontent.socket.emit(request.id, request);
    this.add_tweets(request, response, optioncontent);
    optioncontent.socket.emit(realtime_req.id, realtime_req);
    this.add_tweets(realtime_req, realtime_res, optioncontent);
};

Tweets.prototype.load_trends = function(optioncontent) {
    var trends = utils.attach(optioncontent, hb.trends({id: 'trendscontent', classname: 'trendscontentclass'}));
    optioncontent.socket = optioncontent.parentNode.socket;
    this.get_trends(optioncontent, trends.querySelector('#gettrends'));
};

Tweets.prototype.add_tweets = function(request, response, optioncontent) {
    var handler = function(request, response, optioncontent, chunk) {
	var result = undefined;
	var metadata = undefined;
	var realtime = (request.id.match(/stream/) ? true : false);
	if(chunk.data) {
	    response.push(String.fromCharCode.apply(null, new Uint8Array(chunk.data)));
	    try { result = JSON.parse(response.join('')); } catch(error) { return; }
	    
	    if(result.friends && realtime) {
		response.length = 0;
		return;
	    }
	    if(result.delete) {
		var element = optioncontent.querySelector('#tweet' + result.delete.status.id_str);
		if(element) element.parentNode.removeChild(element);
		response.length = 0;
		return;
	    }
	    if(result.statuses) {
		metadata = result.search_metadata;
		result = result.statuses;
	    }
	    if(!(result instanceof Array)) {
		if(!result.id) return console.log(result);
		result = [result];
	    }
	    result.forEach(this.add_tweet.bind(this, optioncontent, realtime));
	    if(!realtime && result.length) this.add_more_button(optioncontent, request, metadata || result[result.length - 1]);
	    response.length = 0;
	}
    }.bind(this, request, response, optioncontent);
    optioncontent.socket.on(request.id, handler);
};

Tweets.prototype.add_tweet = function(optioncontent, realtime, tweet) {
    var tweets = optioncontent.querySelectorAll('.tweet');
    if(tweets && tweets.length > 199) this.clear_tweets(tweets, 200);
    if(optioncontent.pause) {
	if(!optioncontent.tweetpool) optioncontent.tweetpool = [];
	optioncontent.tweetpool.push(tweet);
	return;
    }
    if(tweet.retweeted_status) {
	tweet.retweeted_status.original_tweet = tweet;
	tweet = tweet.retweeted_status;
    }
    var element = utils.attach(optioncontent,
			       hb.tweet({id: 'tweet' + tweet.id_str, classname: 'tweet', tweet: tweet}),
			       realtime);
    if(element.querySelectorAll('.player').length) plyr.setup();
    element.webObject = tweet;
    tweet.webElement = element;
};

Tweets.prototype.add_more_button = function(optioncontent, request, last) {
    var morebutton = optioncontent.querySelector('.more');
    if(morebutton) morebutton.parentNode.removeChild(morebutton);
    morebutton = document.createElement('a');
    morebutton.className = 'more';
    morebutton.href = "javascript:void(0)";
    morebutton.textContent = 'More..';
    optioncontent.appendChild(morebutton);
    morebutton.addEventListener('click', function(request, socket, last) {
	morebutton.parentNode.removeChild(morebutton);
	request.data = request.data.replace(/\?.*$/, '') + (last.next_results || '?max_id=' + (last.id - 1))
	socket.emit(request.id, request);
    }.bind(this, request, optioncontent.socket, last));
};

Tweets.prototype.get_trends = function(optioncontent, gettrends) {
    gettrends.addEventListener('click', function(optioncontent) {
	var country = optioncontent.querySelector('#countryname').value;
	var request = {id: 'gettrends', data: country};
	var response = [];
	optioncontent.socket.emit(request.id, request);
	optioncontent.socket.on(request.id, function(request, response, optioncontent, chunk) {
	    if(chunk.data) {
		response.push(String.fromCharCode.apply(null, new Uint8Array(chunk.data)));
		try { result = JSON.parse(response.join('')); } catch(error) { return; }
		var trendslist = optioncontent.querySelector('#trendslist');
		var search = optioncontent.querySelector('#search');
		var options = trendslist.querySelectorAll('option');
		if(options) Array.prototype.forEach.call(options, function(option){option.parentNode.removeChild(option);});
		result[0].trends.forEach(function(trendslist, search, trend, index) {
		    var element = document.createElement('option');
		    element.textContent = trend.name;
		    element.value = trend.name;
		    if(index == 0) {
			element.selected = true;
			search.value = element.value;
		    }
		    trendslist.appendChild(element);
		}.bind(this, trendslist, search));
		trendslist.addEventListener('change', function(search, event) {
		    search.value = event.target.value;
		}.bind(this, search));
		this.init_trends_controls(optioncontent);
	    }
	}.bind(this, request, response, optioncontent));
    }.bind(this, optioncontent));
};

Tweets.prototype.init_trends_controls = function(optioncontent) {
    var start = optioncontent.querySelector('#start');
    var pause = optioncontent.querySelector('#pause');
    var resume = optioncontent.querySelector('#resume');
    var stop = optioncontent.querySelector('#stop');

    start.addEventListener('click', function(start, optioncontent) {
	start.disabled = true;
	var tweets = optioncontent.querySelectorAll('.tweet');
	if(tweets) this.clear_tweets(tweets);
	var trendstweets = optioncontent.querySelector('#trendstweets');
	var query = optioncontent.querySelector('#search').value;
	var track = query.match(/[#@]([^\s,]*)/g);
	track = track ? track.map(function(tag){ return tag.substring(1); }).join(',') : query;
	var request = {id: 'search', data: this.api_root + '/search/tweets.json?q=' + encodeURIComponent(query)};
	var realtime_req = {id: 'publicstream', data: this.stream_api_roots['public'] + '?track=' + encodeURIComponent(track)};
	var response = [];
	var realtime_res = [];
	trendstweets.socket = optioncontent.socket;
	trendstweets.socket.emit(request.id, request);
	if(!trendstweets.socket._callbacks[request.id]) this.add_tweets(request, response, trendstweets);
	trendstweets.socket.emit(realtime_req.id, realtime_req);
	if(!trendstweets.socket._callbacks[realtime_req.id]) this.add_tweets(realtime_req, realtime_res, trendstweets);
    }.bind(this, start, optioncontent));
    
    stop.addEventListener('click', function(start, optioncontent) {
	start.disabled = false;
	optioncontent.socket.emit('publicstream', {id: 'publicstream', data: 'disconnect'});
    }.bind(this, start, optioncontent));

    pause.addEventListener('click', function(pause, event) {
	pause.disabled = true;
	optioncontent.querySelector('#trendstweets').pause = true;
    }.bind(this, pause));
    
    resume.addEventListener('click', function(pause, event) {
	pause.disabled = false;
	optioncontent.querySelector('#trendstweets').pause = false;
	var trendstweets = optioncontent.querySelector('#trendstweets');
	if(trendstweets.tweetpool && trendstweets.tweetpool.length) {
	    trendstweets.tweetpool.forEach(this.add_tweet.bind(this, trendstweets, true));
	    trendstweets.tweetpool.length = 0;
	}
    }.bind(this, pause, optioncontent));
};

Tweets.prototype.clear_tweets = function(tweets, count) {
    tweets = Array.prototype.slice.call(tweets, 0, count);
    tweets.forEach(function(tweet) {
	tweet.parentNode.removeChild(tweet);
    }.bind(this));
};

Tweets.prototype.clicked = function() {
    if(!this.rendered) return;
    this.show();
};

Tweets.prototype.show = function() {
    var dmain = document.getElementById('dmain');
    dmain.webObject.hide();
    dmain.webObject = this;
    if(this.view.style.display == 'none') this.view.style.display = 'block';
};

Tweets.prototype.hide = function() {
    this.view.style.display = 'none';
};

module.exports = Tweets;
