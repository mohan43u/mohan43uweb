/*
 * list.js - list widget
 */

var httpclient = require('./httpclient.js');
var hb = require('./hb.js');
var utils = require('./utils.js');

var list = function(props) {
    props = props || {};
    this.classname = props.classname || 'list';
    this.id = props.id || ("list" + (list.count++));
    this.path = props.path || '/objects/unknown';
}

list.count = 0;

list.prototype.render_inside = function(parent, next) {
    httpclient({method: 'GET', uri:this.path, json: true}, function(parent, next, error, response, body) {
	if(!error && response.statusCode == 200) {
	    this.data = body;
	    var items = (this.data instanceof Array ? this.data : Object.keys(this.data));
	    this.view = utils.attach(parent, hb.list({ id: this.id, classname: this.classname, items: items }));
	    this.view.webObject = this;
	    this.init_handlers();
	}
	else {
	    console.warn({error: error, response: response, body: body});
	}
	if(next) next();
    }.bind(this, parent, next));
    return this;
};

list.prototype.init_handlers = function() {
    var items = this.view.querySelectorAll('.item');
    Array.prototype.forEach.call(items, function(item) {
	var action = this.data[item.textContent];
	if(typeof(action) == 'string' && widgets[action]) {
	    var widget = new widgets[action]({id: item.textContent});
	    widget.render_inside(item);
	    item.webObject = widget;
	    if(widget.clicked) {
		item.addEventListener('click', widget.clicked.bind(widget));
	    }
	}
    }.bind(this));
};

module.exports = list;
