var handlebars = require('handlebars');
 module.exports["album"] = handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, lambda=this.lambda;
  return "\n<div id=\""
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\""
    + escapeExpression(((helper = (helper = helpers.classname || (depth0 != null ? depth0.classname : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"classname","hash":{},"data":data}) : helper)))
    + "\">\n	<img src=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.album : depth0)) != null ? stack1.iconLink : stack1), depth0))
    + "\"/>\n	<a id=\"label\" href=\"javascript:void(0)\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.album : depth0)) != null ? stack1.title : stack1), depth0))
    + "</a>\n</div>";
},"useData":true});
module.exports["comment"] = handlebars.template({"1":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "			<a class=\"name\" href=\""
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.comment : depth0)) != null ? stack1.author : stack1)) != null ? stack1.URL : stack1), depth0))
    + "\" target=\"_blank\">";
  stack1 = lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.comment : depth0)) != null ? stack1.author : stack1)) != null ? stack1.name : stack1), depth0);
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</a>\n";
},"3":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, buffer = "			<a class=\"name\">";
  stack1 = lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.comment : depth0)) != null ? stack1.author : stack1)) != null ? stack1.name : stack1), depth0);
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</a>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, lambda=this.lambda, buffer = "\n<div id=\""
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\""
    + escapeExpression(((helper = (helper = helpers.classname || (depth0 != null ? depth0.classname : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"classname","hash":{},"data":data}) : helper)))
    + "\">\n	<div class=\"author\">\n		<div class=\"avatar\" style=\"background:url('"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.comment : depth0)) != null ? stack1.author : stack1)) != null ? stack1.avatar_URL : stack1), depth0))
    + "') center/cover\"></div>\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = ((stack1 = (depth0 != null ? depth0.comment : depth0)) != null ? stack1.author : stack1)) != null ? stack1.URL : stack1), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.program(3, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "	</div>\n	<div class=\"date\">posted: "
    + escapeExpression(((helpers.evaljavascript || (depth0 && depth0.evaljavascript) || helperMissing).call(depth0, "(new Date(param.date)).toLocaleString()", (depth0 != null ? depth0.comment : depth0), {"name":"evaljavascript","hash":{},"data":data})))
    + "</div>\n	<div class=\"content\">";
  stack1 = lambda(((stack1 = (depth0 != null ? depth0.comment : depth0)) != null ? stack1.content : stack1), depth0);
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</div>\n</div>";
},"useData":true});
module.exports["list"] = handlebars.template({"1":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "	<a class=\"item\" href=\"javascript:void(0)\">"
    + escapeExpression(lambda(depth0, depth0))
    + "</a>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "\n<div id=\""
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\""
    + escapeExpression(((helper = (helper = helpers.classname || (depth0 != null ? depth0.classname : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"classname","hash":{},"data":data}) : helper)))
    + "\">\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.items : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</div>";
},"useData":true});
module.exports["picture"] = handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "\n<img id=\""
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\""
    + escapeExpression(((helper = (helper = helpers.classname || (depth0 != null ? depth0.classname : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"classname","hash":{},"data":data}) : helper)))
    + "\" src=\""
    + escapeExpression(((helper = (helper = helpers.small || (depth0 != null ? depth0.small : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"small","hash":{},"data":data}) : helper)))
    + "\" data-jslghtbx=\""
    + escapeExpression(((helper = (helper = helpers.big || (depth0 != null ? depth0.big : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"big","hash":{},"data":data}) : helper)))
    + "\" data-jslghtbx-group=\""
    + escapeExpression(((helper = (helper = helpers.classname || (depth0 != null ? depth0.classname : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"classname","hash":{},"data":data}) : helper)))
    + "\" />";
},"useData":true});
module.exports["post"] = handlebars.template({"1":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "				<a class=\"tag\" href=\"https://mohan43u.wordpress.com/tag/"
    + escapeExpression(lambda((data && data.key), depth0))
    + "\" target=\"_blank\">"
    + escapeExpression(lambda((data && data.key), depth0))
    + "</a>\n";
},"3":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "				<a class=\"category\" href=\"https://mohan43u.wordpress.com/category/"
    + escapeExpression(lambda((data && data.key), depth0))
    + "\" target=\"_blank\">"
    + escapeExpression(lambda((data && data.key), depth0))
    + "</a>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, lambda=this.lambda, buffer = "\n<div id=\""
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\""
    + escapeExpression(((helper = (helper = helpers.classname || (depth0 != null ? depth0.classname : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"classname","hash":{},"data":data}) : helper)))
    + "\">\n	<div class=\"title\"><a href=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.post : depth0)) != null ? stack1.URL : stack1), depth0))
    + "\" target=\"_blank\">";
  stack1 = lambda(((stack1 = (depth0 != null ? depth0.post : depth0)) != null ? stack1.title : stack1), depth0);
  if (stack1 != null) { buffer += stack1; }
  buffer += "</a></div>\n	<div class=\"date\">posted: "
    + escapeExpression(((helpers.evaljavascript || (depth0 && depth0.evaljavascript) || helperMissing).call(depth0, "(new Date(param.date)).toLocaleString()", (depth0 != null ? depth0.post : depth0), {"name":"evaljavascript","hash":{},"data":data})))
    + "</div>\n	<div class=\"modified\">modified: "
    + escapeExpression(((helpers.evaljavascript || (depth0 && depth0.evaljavascript) || helperMissing).call(depth0, "(new Date(param.modified)).toLocaleString()", (depth0 != null ? depth0.post : depth0), {"name":"evaljavascript","hash":{},"data":data})))
    + "</div>\n	<div class=\"content\">";
  stack1 = lambda(((stack1 = (depth0 != null ? depth0.post : depth0)) != null ? stack1.excerpt : stack1), depth0);
  if (stack1 != null) { buffer += stack1; }
  buffer += "</div>\n	<div class=\"tags\">\n		<div class=\"label\">tags</div>\n		<div class=\"data\">\n";
  stack1 = helpers.each.call(depth0, ((stack1 = (depth0 != null ? depth0.post : depth0)) != null ? stack1.tags : stack1), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "		</div>\n	</div>\n	<div class=\"categories\">\n		<div class=\"label\">categories</div>\n		<div class=\"data\">\n";
  stack1 = helpers.each.call(depth0, ((stack1 = (depth0 != null ? depth0.post : depth0)) != null ? stack1.categories : stack1), {"name":"each","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "		</div>\n	</div>\n	<div class=\"comments\">\n		<div class=\"comment_count_label\">comments</div>\n		<div class=\"comment_count\"><a href=\"javascript:void(0)\">"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.post : depth0)) != null ? stack1.discussion : stack1)) != null ? stack1.comment_count : stack1), depth0))
    + "</a></div>\n	</div>\n	<a class=\"reply\" href=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.post : depth0)) != null ? stack1.URL : stack1), depth0))
    + "#respond\" target=\"_blank\">Reply</a>\n</div>";
},"useData":true});
module.exports["trends"] = handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "\n<div id=\""
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\""
    + escapeExpression(((helper = (helper = helpers.classname || (depth0 != null ? depth0.classname : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"classname","hash":{},"data":data}) : helper)))
    + "\">\n	<input id=\"countryname\" type=\"text\" placeholder=\"country to get trends\"/>\n	<button id=\"gettrends\">get trends</button>\n	<select id=\"trendslist\"></select>\n	<input id=\"search\" type=\"text\" placeholder=\"twitter search patterns\"/>\n	<button id=\"start\">start</button>\n	<button id=\"pause\">pause</button>\n	<button id=\"resume\">resume</button>\n	<button id=\"stop\">stop</button>\n	<div id=\"trendstweets\"></div>\n</div>";
},"useData":true});
module.exports["tweet"] = handlebars.template({"1":function(depth0,helpers,partials,data) {
  return "			<div class=\"verified\">Verified: Yes</div>\n";
  },"3":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, buffer = "			<div class=\"retweeted_by\">Retweeted_By: "
    + escapeExpression(lambda(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.tweet : depth0)) != null ? stack1.original_tweet : stack1)) != null ? stack1.user : stack1)) != null ? stack1.name : stack1), depth0))
    + " ";
  stack1 = ((helpers.evaljavascript || (depth0 && depth0.evaljavascript) || helperMissing).call(depth0, "param.user.location.sub()", ((stack1 = (depth0 != null ? depth0.tweet : depth0)) != null ? stack1.original_tweet : stack1), {"name":"evaljavascript","hash":{},"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer + " (<a href=\"https://twitter.com/"
    + escapeExpression(lambda(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.tweet : depth0)) != null ? stack1.original_tweet : stack1)) != null ? stack1.user : stack1)) != null ? stack1.screen_name : stack1), depth0))
    + "\" target=\"_blank\"> @"
    + escapeExpression(lambda(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.tweet : depth0)) != null ? stack1.original_tweet : stack1)) != null ? stack1.user : stack1)) != null ? stack1.screen_name : stack1), depth0))
    + " </a>)</div>\n";
},"5":function(depth0,helpers,partials,data) {
  var stack1, helperMissing=helpers.helperMissing, buffer = "";
  stack1 = ((helpers.xif || (depth0 && depth0.xif) || helperMissing).call(depth0, "param.extended_entities && param.extended_entities.media", (depth0 != null ? depth0.tweet : depth0), {"name":"xif","hash":{},"fn":this.program(6, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  stack1 = ((helpers.xif || (depth0 && depth0.xif) || helperMissing).call(depth0, "!param.extended_entities && param.entities.media", (depth0 != null ? depth0.tweet : depth0), {"name":"xif","hash":{},"fn":this.program(13, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, ((stack1 = ((stack1 = (depth0 != null ? depth0.tweet : depth0)) != null ? stack1.entities : stack1)) != null ? stack1.urls : stack1), {"name":"if","hash":{},"fn":this.program(16, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"6":function(depth0,helpers,partials,data) {
  var stack1, buffer = "			<div class=\"tweetpics\">\n";
  stack1 = helpers.each.call(depth0, ((stack1 = ((stack1 = (depth0 != null ? depth0.tweet : depth0)) != null ? stack1.extended_entities : stack1)) != null ? stack1.media : stack1), {"name":"each","hash":{},"fn":this.program(7, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "			</div>\n";
},"7":function(depth0,helpers,partials,data) {
  var stack1, helperMissing=helpers.helperMissing, buffer = "";
  stack1 = ((helpers.xif || (depth0 && depth0.xif) || helperMissing).call(depth0, "param.type == 'photo' || param.type == 'animated_gif'", depth0, {"name":"xif","hash":{},"fn":this.program(8, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  stack1 = ((helpers.xif || (depth0 && depth0.xif) || helperMissing).call(depth0, "param.type == 'video'", depth0, {"name":"xif","hash":{},"fn":this.program(10, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"8":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "					     	<img class=\"tweetpic\" src=\""
    + escapeExpression(lambda((depth0 != null ? depth0.media_url_https : depth0), depth0))
    + "\"/>\n";
},"10":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "					     	<div class=\"player\">\n							<video poster=\""
    + escapeExpression(lambda((depth0 != null ? depth0.media_url_https : depth0), depth0))
    + "\" controls crossorigin>\n";
  stack1 = helpers.each.call(depth0, ((stack1 = (depth0 != null ? depth0.video_info : depth0)) != null ? stack1.variants : stack1), {"name":"each","hash":{},"fn":this.program(11, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "							</video>\n						</div>\n";
},"11":function(depth0,helpers,partials,data) {
  var helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "									<source src=\""
    + escapeExpression(((helpers.evaljavascript || (depth0 && depth0.evaljavascript) || helperMissing).call(depth0, "\"/cors/\" + btoa(param.url)", depth0, {"name":"evaljavascript","hash":{},"data":data})))
    + "\">\n";
},"13":function(depth0,helpers,partials,data) {
  var stack1, buffer = "			<div class=\"tweetpics\">\n";
  stack1 = helpers.each.call(depth0, ((stack1 = ((stack1 = (depth0 != null ? depth0.tweet : depth0)) != null ? stack1.entities : stack1)) != null ? stack1.media : stack1), {"name":"each","hash":{},"fn":this.program(14, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "			</div>\n";
},"14":function(depth0,helpers,partials,data) {
  var stack1, helperMissing=helpers.helperMissing, buffer = "";
  stack1 = ((helpers.xif || (depth0 && depth0.xif) || helperMissing).call(depth0, "param.type == 'photo' || param.type == 'animated_gif'", depth0, {"name":"xif","hash":{},"fn":this.program(8, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"16":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, ((stack1 = ((stack1 = (depth0 != null ? depth0.tweet : depth0)) != null ? stack1.entities : stack1)) != null ? stack1.urls : stack1), {"name":"each","hash":{},"fn":this.program(17, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"17":function(depth0,helpers,partials,data) {
  var stack1, helperMissing=helpers.helperMissing, buffer = "";
  stack1 = ((helpers.xif || (depth0 && depth0.xif) || helperMissing).call(depth0, "param.expanded_url.match(/youtube.com/)", depth0, {"name":"xif","hash":{},"fn":this.program(18, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  stack1 = ((helpers.xif || (depth0 && depth0.xif) || helperMissing).call(depth0, "param.expanded_url.match(/\\/\\/amp.twimg.com\\/v/)", depth0, {"name":"xif","hash":{},"fn":this.program(20, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"18":function(depth0,helpers,partials,data) {
  var helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "					<div class=\"youtube\">\n						<iframe src=\""
    + escapeExpression(((helpers.evaljavascript || (depth0 && depth0.evaljavascript) || helperMissing).call(depth0, "param.expanded_url.replace(/http[s]*:/g,'').replace(/watch\\?v=/g,'embed/').replace(/&.*$/g, '')", depth0, {"name":"evaljavascript","hash":{},"data":data})))
    + "\" frameborder=\"0\" allowfullscreen></iframe>\n					</div>\n";
},"20":function(depth0,helpers,partials,data) {
  var helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "					<div class=\"twittervideo\">\n						<iframe src=\""
    + escapeExpression(((helpers.evaljavascript || (depth0 && depth0.evaljavascript) || helperMissing).call(depth0, "param.expanded_url.replace(/http[s]*:/g,'').replace(/&.*$/g, '')", depth0, {"name":"evaljavascript","hash":{},"data":data})))
    + "\" frameborder=\"0\" allowfullscreen></iframe>\n					</div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, lambda=this.lambda, buffer = "\n<div id=\""
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\""
    + escapeExpression(((helper = (helper = helpers.classname || (depth0 != null ? depth0.classname : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"classname","hash":{},"data":data}) : helper)))
    + "\">\n     	<img class=\"profilepic\" src=\""
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.tweet : depth0)) != null ? stack1.user : stack1)) != null ? stack1.profile_image_url_https : stack1), depth0))
    + "\"/>\n	<div class=\"user\">\n		<div class=\"name\">Name: "
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.tweet : depth0)) != null ? stack1.user : stack1)) != null ? stack1.name : stack1), depth0))
    + " ";
  stack1 = ((helpers.evaljavascript || (depth0 && depth0.evaljavascript) || helperMissing).call(depth0, "param.user.location.sub()", (depth0 != null ? depth0.tweet : depth0), {"name":"evaljavascript","hash":{},"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += " (<a href=\"https://twitter.com/"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.tweet : depth0)) != null ? stack1.user : stack1)) != null ? stack1.screen_name : stack1), depth0))
    + "\" target=\"_blank\"> @"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.tweet : depth0)) != null ? stack1.user : stack1)) != null ? stack1.screen_name : stack1), depth0))
    + " </a>)</div>\n		<div class=\"description\">Description: ";
  stack1 = ((helpers.evaljavascript || (depth0 && depth0.evaljavascript) || helperMissing).call(depth0, "(param.user && param.user.description ? param.user.description.replace(/http[s]*:\\/\\/[^\\s]*/g, '<a href=\"$&\" target=\"_blank\">$&</a>').replace(/@([^\\s]*)/g, '<a href=\"https://twitter.com/$1\" target=\"_blank\">$&</a>').replace(/#([^\\s]*)/g, '<a href=\"https://twitter.com/#$1\" target=\"_blank\">$&</a>') : undefined)", (depth0 != null ? depth0.tweet : depth0), {"name":"evaljavascript","hash":{},"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += "</div>\n		<div class=\"postedtime\">Posted: "
    + escapeExpression(((helpers.evaljavascript || (depth0 && depth0.evaljavascript) || helperMissing).call(depth0, "(new Date(param.created_at)).toLocaleString()", (depth0 != null ? depth0.tweet : depth0), {"name":"evaljavascript","hash":{},"data":data})))
    + "</div>\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = ((stack1 = (depth0 != null ? depth0.tweet : depth0)) != null ? stack1.user : stack1)) != null ? stack1.verified : stack1), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "		<div class=\"source\">Source: ";
  stack1 = ((helpers.evaljavascript || (depth0 && depth0.evaljavascript) || helperMissing).call(depth0, "(param.source ? param.source.replace(/\\<(a[^>]*)\\>/, '<$1 target=\"_blank\">') : undefined)", (depth0 != null ? depth0.tweet : depth0), {"name":"evaljavascript","hash":{},"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += "</div>\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.tweet : depth0)) != null ? stack1.original_tweet : stack1), {"name":"if","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "		<div class=\"link\"><a href=\"https://twitter.com/statuses/"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.tweet : depth0)) != null ? stack1.id_str : stack1), depth0))
    + "\" target=\"_blank\">link</a></div>\n	</div>\n	<div class=\"text\">";
  stack1 = ((helpers.evaljavascript || (depth0 && depth0.evaljavascript) || helperMissing).call(depth0, "(param.text ? param.text.replace(/http[s]*:\\/\\/[^\\s]*/g, '<a href=\"$&\" target=\"_blank\">$&</a>').replace(/@([^\\s]*)/g, '<a href=\"https://twitter.com/$1\" target=\"_blank\">$&</a>').replace(/#([^\\s]*)/g, '<a href=\"https://twitter.com/#$1\" target=\"_blank\">$&</a>') : undefined)", (depth0 != null ? depth0.tweet : depth0), {"name":"evaljavascript","hash":{},"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += "</div>\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.tweet : depth0)) != null ? stack1.entities : stack1), {"name":"if","hash":{},"fn":this.program(5, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</div>";
},"useData":true});