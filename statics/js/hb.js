/*
 * hb.js - wrap hbstatic.js with helpers
 */

var handlebars = require('handlebars');
var hbstatic = require('./hbstatic.js');
var registered = false;

var register_evaljavascript = function() {
    handlebars.registerHelper('evaljavascript', function(js, param) { return eval(js); });
};
var register_xif = function() {
    handlebars.registerHelper('xif', function(js, param, options) { if(eval(js)) { return options.fn(this); } return options.inverse(this); });
};

var register_helpers = function() {
    if(!registered) {
	register_evaljavascript();
	register_xif();
	registered = true;
    }
};

register_helpers();
module.exports = hbstatic;
