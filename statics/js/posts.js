/*
 * posts.js - post view
 */

var httpclient = require('./httpclient.js');
var hb = require('./hb.js');
var utils = require('./utils.js');

var Posts = function(props) {
    props = props || {};
    this.classname = props.classname || "posts";
    this. id = props.id || ("posts" + (Posts.count++));
    this.path = props.path || "/wordpress/me";
};

Posts.count = 0;

Posts.prototype.render_inside = function(parent, next) {
    var dmain = document.getElementById('dmain');
    this.view = utils.attach(dmain);
    this.view.id = this.id;
    this.view.className = this.classname;
    this.view.webObject = this;
    if(!dmain.webObject) dmain.webObject = this;
    if(dmain.webObject != this) this.hide();
    httpclient({method: 'GET', uri: this.path, json: true}, function(next, error, response, body) {
	if(!error && response.statusCode == 200) {
	    this.me = body;
	    this.get_posts('/wordpress/sites/' + this.me.primary_blog + '/posts', next);
	}
	else {
	    console.warn({error: error, response: response, body: body});
	}
    }.bind(this, next));
    return this;
};

Posts.prototype.get_posts = function(uri, next) {
    httpclient({method: 'GET', uri: uri, json: true}, function(next, error, response, body) {
	if(!error && response.statusCode == 200) {
	    this.posts = body;
	    this.posts.posts.forEach(function(post) {
		var element = utils.attach(this.view, hb.post({ id: 'post' + post.ID, classname: "post", post: post }));
		this.add_continue_reading_button(element);
		this.add_comments(element);
		element.webObject = post;
	    }.bind(this));
	    this.rendered = true;
	    this.add_more_button();
	    if(next) next();
	}
	else {
	    console.warn({error: error, response: response, body: body});
	}
    }.bind(this, next));
};

Posts.prototype.add_continue_reading_button = function(element) {
    var cr = document.createElement('a');
    var content = element.querySelector('.content');
    cr.className = 'cr';
    cr.href = "javascript:void(0)";
    cr.textContent = '[continue reading..]';
    content.innerHTML = content.innerHTML.replace(/...<\/p>/, cr.outerHTML);
    element.querySelector('.cr').addEventListener('click', function(element, event) {
	element.querySelector('.content').innerHTML = element.webObject.content;
	this.handle_gists(element);
	this.remove_background_for_prettify(element);
	PR.prettyPrint();
    }.bind(this, element));
};

Posts.prototype.remove_background_for_prettify = function(element) {
    Array.prototype.forEach.call(element.querySelectorAll('pre'), function(pre) {
	pre.className = "prettyprint";
	pre.style.removeProperty('background');
    });
};

Posts.prototype.clicked = function() {
    if(!this.rendered) return;
    this.show();
};

Posts.prototype.show = function() {
    var dmain = document.getElementById('dmain');
    dmain.webObject.hide();
    dmain.webObject = this;
    if(this.view.style.display == 'none') this.view.style.display = 'block';
};

Posts.prototype.hide = function() {
    this.view.style.display = 'none';
};

Posts.prototype.add_more_button = function() {
    if(this.posts && this.posts.meta && this.posts.meta.next_page) {
	var morebutton = document.createElement('a');
	morebutton.className = 'more';
	morebutton.href = "javascript:void(0)";
	morebutton.textContent = 'Older Posts..';
	this.view.appendChild(morebutton);
	morebutton.addEventListener('click', function(morebutton) {
	    this.view.removeChild(morebutton);
	    var uri = '/wordpress/sites/' + this.me.primary_blog + '/posts';
	    uri += '?page_handle=' + encodeURIComponent(this.posts.meta.next_page);
	    this.get_posts(uri);
	}.bind(this, morebutton));
    }
};

Posts.prototype.add_comments = function(element) {
    var parent = element.querySelector('.comments');
    var comment_count = element.querySelector('.comment_count');
    if(parseInt(comment_count.textContent) > 0) {
	comment_count.addEventListener('click', function(parent, comment_count, event) {
	    parent.removeChild(comment_count);
	    var comments = new widgets['comments']();
	    comments.render_inside(parent);
	}.bind(element, parent, comment_count));
    }
};

Posts.prototype.handle_gists = function(element) {
    Array.prototype.forEach.call(element.querySelectorAll('script[src*="gist.github.com"]'), function(script) {
	httpclient({method: 'GET', uri: script.src}, function(script, error, response, body) {
	    if(!error && response.statusCode == 200) {
		lines = body.replace(/document.write\('(.*)'\)/gm, '$1').split('\n');
		var html = lines[0] + JSON.parse('{"string": "' + lines[1].replace(/\t/g, '') + '"}').string;
		script.outerHTML = html;
	    }
	    else {
		console.warn({error: error, response: response, body: body});
	    }
	}.bind(this, script));
    }.bind(this));
};

module.exports = Posts;
