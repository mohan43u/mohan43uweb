{{! album }}

<div id="{{id}}" class="{{classname}}">
	<img src="{{album.iconLink}}"/>
	<a id="label" href="javascript:void(0)">{{album.title}}</a>
</div>