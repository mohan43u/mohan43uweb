{{! post }}

<div id="{{id}}" class="{{classname}}">
	<div class="title"><a href="{{post.URL}}" target="_blank">{{{post.title}}}</a></div>
	<div class="date">posted: {{evaljavascript "(new Date(param.date)).toLocaleString()" post}}</div>
	<div class="modified">modified: {{evaljavascript "(new Date(param.modified)).toLocaleString()" post}}</div>
	<div class="content">{{{post.excerpt}}}</div>
	<div class="tags">
		<div class="label">tags</div>
		<div class="data">
			{{#each post.tags}}
				<a class="tag" href="https://mohan43u.wordpress.com/tag/{{@key}}" target="_blank">{{@key}}</a>
			{{/each}}
		</div>
	</div>
	<div class="categories">
		<div class="label">categories</div>
		<div class="data">
			{{#each post.categories}}
				<a class="category" href="https://mohan43u.wordpress.com/category/{{@key}}" target="_blank">{{@key}}</a>
			{{/each}}
		</div>
	</div>
	<div class="comments">
		<div class="comment_count_label">comments</div>
		<div class="comment_count"><a href="javascript:void(0)">{{post.discussion.comment_count}}</a></div>
	</div>
	<a class="reply" href="{{post.URL}}#respond" target="_blank">Reply</a>
</div>