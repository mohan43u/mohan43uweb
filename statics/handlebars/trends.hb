{{! trends }}

<div id="{{id}}" class="{{classname}}">
	<input id="countryname" type="text" placeholder="country to get trends"/>
	<button id="gettrends">get trends</button>
	<select id="trendslist"></select>
	<input id="search" type="text" placeholder="twitter search patterns"/>
	<button id="start">start</button>
	<button id="pause">pause</button>
	<button id="resume">resume</button>
	<button id="stop">stop</button>
	<div id="trendstweets"></div>
</div>