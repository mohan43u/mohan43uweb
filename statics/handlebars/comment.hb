{{! comment }}

<div id="{{id}}" class="{{classname}}">
	<div class="author">
		<div class="avatar" style="background:url('{{comment.author.avatar_URL}}') center/cover"></div>
		{{#if comment.author.URL}}
			<a class="name" href="{{comment.author.URL}}" target="_blank">{{{comment.author.name}}}</a>
		{{else}}
			<a class="name">{{{comment.author.name}}}</a>
		{{/if}}		
	</div>
	<div class="date">posted: {{evaljavascript "(new Date(param.date)).toLocaleString()" comment}}</div>
	<div class="content">{{{comment.content}}}</div>
</div>