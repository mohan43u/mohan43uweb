{{! tweet }}

<div id="{{id}}" class="{{classname}}">
     	<img class="profilepic" src="{{tweet.user.profile_image_url_https}}"/>
	<div class="user">
		<div class="name">Name: {{tweet.user.name}} {{{evaljavascript "param.user.location.sub()" tweet}}} (<a href="https://twitter.com/{{tweet.user.screen_name}}" target="_blank"> @{{tweet.user.screen_name}} </a>)</div>
		<div class="description">Description: {{{evaljavascript "(param.user && param.user.description ? param.user.description.replace(/http[s]*:\/\/[^\s]*/g, '<a href=\"$&\" target=\"_blank\">$&</a>').replace(/@([^\s]*)/g, '<a href=\"https://twitter.com/$1\" target=\"_blank\">$&</a>').replace(/#([^\s]*)/g, '<a href=\"https://twitter.com/#$1\" target=\"_blank\">$&</a>') : undefined)" tweet}}}</div>
		<div class="postedtime">Posted: {{evaljavascript "(new Date(param.created_at)).toLocaleString()" tweet}}</div>
		{{#if tweet.user.verified}}
			<div class="verified">Verified: Yes</div>
		{{/if}}
		<div class="source">Source: {{{evaljavascript "(param.source ? param.source.replace(/\<(a[^>]*)\>/, '<$1 target=\"_blank\">') : undefined)" tweet}}}</div>
		{{#if tweet.original_tweet}}
			<div class="retweeted_by">Retweeted_By: {{tweet.original_tweet.user.name}} {{{evaljavascript "param.user.location.sub()" tweet.original_tweet}}} (<a href="https://twitter.com/{{tweet.original_tweet.user.screen_name}}" target="_blank"> @{{tweet.original_tweet.user.screen_name}} </a>)</div>
		{{/if}}
		<div class="link"><a href="https://twitter.com/statuses/{{tweet.id_str}}" target="_blank">link</a></div>
	</div>
	<div class="text">{{{evaljavascript "(param.text ? param.text.replace(/http[s]*:\/\/[^\s]*/g, '<a href=\"$&\" target=\"_blank\">$&</a>').replace(/@([^\s]*)/g, '<a href=\"https://twitter.com/$1\" target=\"_blank\">$&</a>').replace(/#([^\s]*)/g, '<a href=\"https://twitter.com/#$1\" target=\"_blank\">$&</a>') : undefined)" tweet}}}</div>
	{{#if tweet.entities}}
		{{#xif "param.extended_entities && param.extended_entities.media" tweet}}
			<div class="tweetpics">
				{{#each tweet.extended_entities.media}}
					{{#xif "param.type == 'photo' || param.type == 'animated_gif'" this}}
					     	<img class="tweetpic" src="{{this.media_url_https}}"/>
					{{/xif}}
					{{#xif "param.type == 'video'" this}}
					     	<div class="player">
							<video poster="{{this.media_url_https}}" controls crossorigin>
							       {{#each this.video_info.variants}}
									<source src="{{evaljavascript '"/cors/" + btoa(param.url)' this}}">
							       {{/each}}
							</video>
						</div>
					{{/xif}}
				{{/each}}
			</div>
		{{/xif}}
		{{#xif "!param.extended_entities && param.entities.media" tweet}}
			<div class="tweetpics">
				{{#each tweet.entities.media}}
					{{#xif "param.type == 'photo' || param.type == 'animated_gif'" this}}
					     	<img class="tweetpic" src="{{this.media_url_https}}"/>
					{{/xif}}
				{{/each}}
			</div>
		{{/xif}}
		{{#if tweet.entities.urls}}
			{{#each tweet.entities.urls}}
				{{#xif "param.expanded_url.match(/youtube.com/)" this}}
					<div class="youtube">
						<iframe src="{{evaljavascript "param.expanded_url.replace(/http[s]*:/g,'').replace(/watch\?v=/g,'embed/').replace(/&.*$/g, '')" this}}" frameborder="0" allowfullscreen></iframe>
					</div>
				{{/xif}}
				{{#xif "param.expanded_url.match(/\/\/amp.twimg.com\/v/)" this}}
					<div class="twittervideo">
						<iframe src="{{evaljavascript "param.expanded_url.replace(/http[s]*:/g,'').replace(/&.*$/g, '')" this}}" frameborder="0" allowfullscreen></iframe>
					</div>
				{{/xif}}
			{{/each}}
		{{/if}}
	{{/if}}
</div>